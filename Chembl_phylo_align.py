#!usr/bin/python
#Status : Crash

from chembl_webresource_client import *
from Bio import Seq, SeqIO, SwissProt, AlignIO, Phylo
from Bio.Align.Applications import ClustalwCommandline
import pc_for_disease as chem


def Write_fasta_file(disease):
	"""This function writes the target binding protein sequences in 
	   fasta file for the matching ChEMBL Ids.
	"""
	chebi_list = chem.input_disease_name(disease)
	chembl_list = chem.access_ChEBI_and_ChEMBLid(chebi_list)
	sequences = []
	input_handle = open("chembl_21.fa","rU")
	output_handle = open("my_chembl.fa","w")
	ids = []
	print chembl_list
	for i in range(len(chembl_list)):
		for record in SeqIO.parse(input_handle, "fasta"):
			ids.append(record.id)
			if (record.id in chembl_list):
				sequences.append(record)
				print record.id
	for id in ids:
		output_handle.write(id + "\n")
		if id in chembl_list:
			print id
	
	SeqIO.write(sequences, output_handle, "fasta") 		
	output_handle.close()
	input_handle.close()
	

def align_with_ClustalW():
	"""This function aligns the fasta file input with ClustalW
	"""
	in_file = "p.fasta"
	out_file = "r2_clustalO"
	clustalw_cline = ClustalwCommandline("clustalw", infile = "p6.fasta")
	clustalw_cline()

	
def representing_data():
	"""This function generates a sequence alignment and a phylogenetic tree
	 for the clustal alignment generated.
	"""
	align = AlignIO.read("p.aln","clustal")
	print align
	tree = Phylo.read("p.dnd","newick")
	Phylo.draw_ascii(tree)
	
	
Write_fasta_file("leukemia")
align_with_ClustalW()
representing_data()