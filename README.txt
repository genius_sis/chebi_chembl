This README file is intended for highlighting the applications of the script.
The main application of the python script is to generate a sorted list of 
CHEMBL compound ids and their partition coefficients(logP values) in increasing
order for any input disease. The logarithmic value of the partition coefficient
is an important value which helps in the measure of lipophilicity of a drug and
in the calculation of IC50 of the drugs. The logP values are also used in 
calculation of distribution coefficient and logD values which helps us in 
analysing how easily the drug can reach its intended target in the body, how 
strong an effect it will have once it reaches its target and how long it will
remain in the body in active form.

The .tar file contains two working python scripts called pc_for_disease.py and  
coursework_main.py script. The python script pc_for_disease.py can be accessed 
via command line tools. To increase the back-end a python script 
(Chembl_phylo_align.py) was written which could access the target binding sites
for the given ChEMBL Ids but it couldn't work as the ChEMBL fasta file didn't 
contain record details for those particular ChEMBL Ids. On the front end, 
efforts were made to integrate the script with Django, but due to limited time, 
successful integration of python scripts to the Django app has been a 
shortcoming.

The access to git for these scripts can be accessed at 
https://genius_sis@bitbucket.org/genius_sis/chebi_chembl.git

A successful unit testing script couldn't be set up for this script as for some
cases the ChEBI database didn't have any data resulting in void. For example, 
bronchiolitis. Also, even if exception cases are set up for disease names that 
work and disease they don't work for, the data from the web will eventually 
change in the coming time which will make the unit testing setup for this 
script obsolete.

The resulting ChEMBL id lists can be seen to be reduced in the data generated 
as compared to the data input from the ChEBI IDs. This occurs when ChEBI IDs 
are mapped onto ChEMBL IDs due to inconsistency in the data mapping between 
ChEBI and ChEMBL databases.


Python Requirements
===================
The application runs on Python 2.7 or higher.


Dependencies
============
Few dependencies are installed for the working of the application. First insure
that Python is installed correctly. Then installation of Biopython is 
recommended as the script uses certain packages which which are already present
in Biopython. Installation of other dependencies include :
1) bioservices
bioservices can be installed easily by using pip in sudo mode.
Use the following command to install bioservices:
pip install bioservices

2) chembl_webresource_client
chembl_webresource_client is a part of the ChEMBL web services and can be 
installed easily by using pip in sudo mode.
Use the following command to install chembl_webresource_client:
pip install chembl_webresource_client


Use case
========
A string argument which is the name of a disease is supplied in the 
coursework_main.py file.
In this case, leukemia has been used an example.

>>>import pc_for_disease as part_coeff

>>>part_coeff.Calculate_PC("leukemia")

The expected output(list format) for this example is :
 
ChEMBL ID compounds along with their partition coefficients are:
CHEMBL1551724 -2.91
CHEMBL1096882 -1.38
CHEMBL502751 -1.2
CHEMBL820 -0.29
CHEMBL2407403 1.91
CHEMBL2407404 1.91
CHEMBL540307 3.19
CHEMBL1421 3.44
CHEMBL2216870 3.62
CHEMBL515 3.9
CHEMBL1642 4.22
CHEMBL941 4.22
CHEMBL465821 4.42
CHEMBL499239 4.78
CHEMBL443857 4.8
CHEMBL455797 4.8
CHEMBL1774396 5.01
CHEMBL561363 5.06
CHEMBL521314 5.23
CHEMBL466975 5.59
CHEMBL513011 5.59
CHEMBL487104 5.88
CHEMBL496451 6.12
CHEMBL464771 6.69


