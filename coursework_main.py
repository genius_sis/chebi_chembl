#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  8 13:43:36 2016

@author: anukrati_nigam
"""
## This python file imports the pc_for_disease.py script to use it for further 
## processing.
import pc_for_disease as part_coeff
"""  Example : 
	A string argument which is the name of a disease is supplied in the 
	coursework_main.py file.
	In this case, leukemia has been used an example.
	
	>>>import pc_for_disease as part_coeff
	
	>>>part_coeff.Calculate_PC("leukemia")
	
	The expected output(list format) for this example is :
	 
	ChEMBL ID compounds along with their partition coefficients are:
	CHEMBL1551724 -2.91
	CHEMBL1096882 -1.38
	CHEMBL502751 -1.2
	CHEMBL820 -0.29
	CHEMBL2407403 1.91
	CHEMBL2407404 1.91
	CHEMBL540307 3.19
	CHEMBL1421 3.44
	CHEMBL2216870 3.62
	CHEMBL515 3.9
	CHEMBL1642 4.22
	CHEMBL941 4.22
	CHEMBL465821 4.42
	CHEMBL499239 4.78
	CHEMBL443857 4.8
	CHEMBL455797 4.8
	CHEMBL1774396 5.01
	CHEMBL561363 5.06
	CHEMBL521314 5.23
	CHEMBL466975 5.59
	CHEMBL513011 5.59
	CHEMBL487104 5.88
	CHEMBL496451 6.12
	CHEMBL464771 6.69
"""
part_coeff.Calculate_PC("leukemia")

