from django.contrib import admin

# Register your models here.
from .models import Question_disease_name, Choice

class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3


class Question_disease_nameAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]
    list_display = ('question_text', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['question_text']


admin.site.register(Question_disease_name, Question_disease_nameAdmin)
