#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  6 14:31:02 2016

@author: anukrati_nigam
"""
"""Provides the partition coefficients and ChEMBL compound Ids for a given 
   disease name.
"""
from bioservices import *
import os
from bioservices.services import REST
import webbrowser
from chembl_webresource_client import *

ch = ChEBI()
uni = UniChem()
compounds = CompoundResource()

def input_disease_name(disease):
	""" This function generates ChEBI Id lists for the disease name that the
	 information is required.
		
	The function takes in one argument which is the disease name and returns a
	list of ChEBI ids with only their numbers.
	"""
	#getLiteEntity function accessses the data for the disease name in the ChEBI
	#database  
	res = ch.getLiteEntity(disease)
	chebi_id_list = []
	for i in range(len(res)):
		chebi_id_list.append(res[i][0]) 
	chebi_id_list_stripped = []	
	#The list generated is stripped of the CHEBI: identifier so that it can be 
	#mapped onto the CHEMBL database. As for mapping only numbers are accepted 
	#in the ChEBI identifiers.
	for i in range(len(chebi_id_list)):
		chebi_id_list_stripped.append(str(chebi_id_list[i].lstrip('CHEBI:')))
	return chebi_id_list_stripped


def access_ChEBI_and_ChEMBLid(cils):
	"""This function maps the ChEBI ids to the ChEMBL ids using the 
	bioservices package in Biopython.
 
	The function takes in the ChEBI 	id lists with only numbers as an argument
	and maps all the ChEMBL ids corresponding to the particular ChEBI ids. It 
	returns a list of the available ChEMBL IDs corresponding to our disease 
	name.
	""" 
	#The get_mapping accesses all the ChEBI Ids and the ChEMBL Ids that have 
	#been mapped in their databases.
	mapping = uni.get_mapping("chebi","chembl")
	mapping_list = []
	#The 	
	for i in range(len(cils)):
		for key in mapping:
			if cils[i] == key:
				mapping_list.append(str(mapping[key]))
	return mapping_list
	

def generate_partition_coeff(chembl_id_mapping_list):
	"""This function generates the partition coefficient (logP) values for the 
	ChEMBL Ids generated for the input disease. 
	
	This function takes in the 	ChEMBL id lists as an argument and returns a 
	sorted list of ChEMBL id compounds and their partition coefficients in an 
	inceasing order, sorted on the basis of the partition coefficients.
	"""
	partition_coeff_list = []
	sorted_list_part_coeff = []	
	#The get function gets all the values available for the ChEMBL Id and only 
	#the partition coefficient value(alogp) is accessed. 
	for i in range(len(chembl_id_mapping_list)):
		a = compounds.get(chembl_id_mapping_list[i])
		partition_coeff_list.append(a['alogp'])
				
	merged_list_chembl_id_part_coeff = zip(chembl_id_mapping_list,
								partition_coeff_list)
	sorted_list_part_coeff = sorted(merged_list_chembl_id_part_coeff,
							key=lambda x: x[1] )
	print "ChEMBL ID compounds along with their partition coefficients are:"
	for id in sorted_list_part_coeff:
		print "{} {}".format(id[0], id[1])


def Calculate_PC(disease):
	"""This function runs the whole script eventually generating ChEMBL ID 
	compounds along with their partition coefficients for any given disease
	"""
	chebi_stripped = input_disease_name(disease)
	mapping_chebi_list = access_ChEBI_and_ChEMBLid(chebi_stripped)
	generate_partition_coeff(mapping_chebi_list)
