from django.conf.urls import url


from . import views

app_name = 'ChebiChembl'
urlpatterns = [
    url(r'^index/$',views.IndexView.as_view(),name="index"),
    url(r'^results/$',views.ResultsView.as_view(),name="results"),
    url(r'^hello_world/$', views.hello_world),
]