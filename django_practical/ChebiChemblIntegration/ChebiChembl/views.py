from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic
from django.utils import timezone
from .forms import NameForm
from django.shortcuts import render_to_response, redirect

from .models import Choice, Question_disease_name
import pc_for_disease as pc
import hello as h




from django.http import HttpResponse

def hello_world(request):
    return HttpResponse("Hello, world.")

def disease(request):
        """Generate sorted list of partition coefficients"""
        temp = h.world()
        return HttpResponse("hELLO")


def submit(request):
    info=request.POST['info']
    return render(request, "ChebiChembl/results.html", {"answer": "Hello world"})


def get_name(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NameForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            mymodel = pc.Calculate_PC()

            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect('/thanks/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = NameForm()

    return render(request, 'name.html', {'form': form})

class IndexView(generic.ListView):
    template_name = 'ChebiChembl/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions (not including those set to be
        published in the future)."""
        
        return Question_disease_name.objects.filter(
            pub_date__lte=timezone.now()
        ).order_by('-pub_date')[:5]


    def get_name(request):
        # if this is a POST request we need to process the form data
        if request.method == 'POST':
        # create a form instance and populate it with data from the request:
            form = NameForm(request.POST)
        # check whether it's valid:
            if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
                return HttpResponseRedirect('/thanks/')

    # if a GET (or any other method) we'll create a blank form
        else:
            form = NameForm()

        return render_to_response("name.html",  {'form': form,  }, context_instance = RequestContext(request))



class ResultsView(generic.ListView):
    model = Question_disease_name
    template_name = 'ChebiChembl/results.html'

    def disease(request):
        """Generate sorted list of partition coefficients"""
        temp = h.world()
        return render(request, "ChebiChembl/results.html", {"answer": temp})
    #def get_partition_coeff(self):


